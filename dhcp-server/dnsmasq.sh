#!/bin/bash -x

# install dnsmasq
apt install --yes --install-recommends dnsmasq

# fix dnsmasq configuration
sed -i /etc/dnsmasq.conf -e '/^dhcp-range/d'
echo "dhcp-range=192.168.11.100,192.168.11.250,8h" >> /etc/dnsmasq.conf
service dnsmasq restart
