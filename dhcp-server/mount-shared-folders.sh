#!/bin/bash -x
# mount shared folders on reboot

sed -i /etc/fstab -e '/^host/d'

cat <<EOF >> /etc/fstab
host /host vboxsf defaults 0 0
EOF
