#!/bin/bash -x
### Setup networking.

sed -i /etc/network/interfaces -e '/^auto enp0s3/,$ d'
cat <<EOF >> /etc/network/interfaces
auto enp0s3
iface  enp0s3 inet dhcp

auto enp0s8
iface enp0s8 inet static
    address 192.168.11.1
    netmask 255.255.255.0
EOF
