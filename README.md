# Virtual LTSP

## Description

[LTSP](http://www.ltsp.org/) allows computers of a LAN to boot through
the network from a single server. LTSP server can be set up on a
VirtualBox as well. This project automates installation and
configuration of a LTSP server with vagrant and VirtualBox.

LTSP server has two main modes of operation: normal and gateway.  This
depends on whether we have a Gateway/DHCP server in the LAN or not.

1. **Normal** means that there is already a gateway and DHCP server on
   the LAN and the LTSP server provides only booting information.  It
   is called *normal* because this is most often the case.

1. **Gateway** means that the LTSP server is also the gateway of
   the LAN and provides DHCP service to the clients.

For more information on modes of operation see: [LTSP Server
Modes](https://gitlab.com/Virtual-LTSP/VirtualBox/wikis/LTSP-Server-Modes)


## Dependencies

Virtual LTSP depends on [virtualbox](https://www.virtualbox.org/) and
[vagrant](https://www.vagrantup.com/) (>=2.0.2). On Debian9 they can
be installed like this:

```bash
echo "deb http://ftp.debian.org/debian stretch-backports main contrib" \
    > /etc/apt/sources.list.d/stretch-backports.list
apt update
apt install -t stretch-backports virtualbox vagrant
```


## Starting the LTSP server

- Get the scripts:
  ```
  git clone --branch bionic https://gitlab.com/Virtual-LTSP/VirtualBox ltsp
  cd ltsp/
  ```
- Edit `settings.sh` and make sure to set `GATEWAY` to `yes` or
  `no`, and `LAN_IF` to the interface that is connected to the LAN.
- Use `./server.sh build` to build the server. This takes a lot of
  time downloading images and installing packages, so be patient.
- Use `./server.sh halt` to stop the server and `./server.sh up` to
  start it again.


## Starting the LTSP client

You can start a LTSP client by booting a machine from the network. Or
you can run it in a virtual machine like this:
```bash
wget -O ltsp-client.sh \
     https://gitlab.com/Virtual-LTSP/VirtualBox/raw/bionic/scripts/ltsp-client.sh
chmod +x ltsp-client.sh
./ltsp-client.sh start --lan-if=enp1s0f0 --memory=1024
```
Note: To create manually a virtual machine client see this wiki page:
https://gitlab.com/Virtual-LTSP/VirtualBox/wikis/Create-LTSP-Client-Manually


## Development and testing

For development and testing make sure to set `DEVELOPMENT="yes"` on
`settings.sh`.

For more details about the test cases see:
https://gitlab.com/Virtual-LTSP/VirtualBox/wikis/Test-Cases


## Backup and restore user accounts

The script `scripts/users.sh` can be used to manage user accounts. However
it shoud be called from inside the ltsp-server (virtualbox
machine). It can be used like this:
```bash
cd ltsp-server/
vagrant ssh
sudo su
cd /host/
scripts/users.sh
```

## Customize installation

Depending on the vagrant box that you use, you may need to do a few
customizations before the installation script
(`ltsp-server/install.sh`) starts.  Also you may need to do a few
changes after installation is finished.  To automate these changes you
can use the hooks `ltsp-server/pre-install.sh` and
`ltsp-server/post-install.sh`. To ignore the changes on these files
use:
```
git update-index --skip-worktree ltsp-server/{pre,post}-install.sh
```
