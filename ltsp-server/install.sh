#!/bin/bash -x

set -e    # stop on error

# auxiliary functions
include() {
    local script=$1
    source $SRC_DIR/$script
}
run() {
    local script=$1; shift
    $SRC_DIR/scripts/$script "$@"
}
is_gateway() {
    [[ ${GATEWAY,,} == yes ]]
}
export -f include run is_gateway

# main function
main() {
    export SRC_DIR="$(realpath ${SRC_DIR:-/host})"
    export DEBIAN_FRONTEND=noninteractive

    include settings.sh

    # run a hook script before the installation
    include ltsp-server/pre-install.sh

    # build the client image and configure the server
    build_ltsp_client_image
    setup_ltsp_server

    # run a hook script after the installation
    include ltsp-server/post-install.sh
}

build_ltsp_client_image() {
    # add repository ppa:ts.sch.gr
    add-apt-repository ppa:ts.sch.gr --yes
    apt update
    echo "network-manager-config-connectivity-ubuntu hold" | dpkg --set-selections
    apt upgrade --yes

    # install ltsp packages
    apt install --yes --install-recommends \
        ltsp-server ltsp-client epoptes

    # update kernel
    sed -i /etc/ltsp/update-kernels.conf \
        -e '/^IPAPPEND=/ c IPAPPEND=3'
    /usr/share/ltsp/update-kernels

    # install additional packages that are needed on the client
    apt install --yes --install-recommends $PACKAGES

    # create admin
    local user=${ADMIN_USER:-admin}
    local pass=${ADMIN_PASS:-pass}
    run create-admin-account.sh $user "$pass"

    # allow users to change the password from the terminal
    # needs also the setting REMOTE_APPS=True on lts.conf
    sed -i /etc/skel/.bashrc -e '/alias passwd=/d'
    echo "alias passwd='ltsp-remoteapps mate-terminal -e /usr/local/bin/passwd.sh'" >> /etc/skel/.bashrc
    cat <<EOF > /usr/local/bin/passwd.sh
#!/bin/bash
passwd
read -p "Press ENTER to close the terminal..."
EOF
    chmod +x /usr/local/bin/passwd.sh

    # create client image
    ltsp-update-image --cleanup --exclude="${SRC_DIR#/}" /

    # create a chroot directory
    local file_img="$(echo /opt/ltsp/images/*.img)"
    local dir=$(basename ${file_img%.img})
    unsquashfs -d /opt/ltsp/$dir $file_img
    rm /opt/ltsp/$dir/etc/resolv.conf
}

setup_ltsp_server() {
    setup_hostname
    setup_networking
    is_gateway && enable_nat
    mount_vbox_shared_folders
    setup_lts_conf
    setup_dnsmasq
    setup_lightdm
    setup_users
    [[ -n $GUAC_ADMIN ]] && run install-guacamole.sh
    install_chat
    cleanup
    apt upgrade --yes
    echo "network-manager-config-connectivity-ubuntu install" | dpkg --set-selections
}

setup_hostname() {
    local hostname=${HOSTNAME:-ltsp}
    hostname $hostname
    echo $hostname > /etc/hostname
    sed -i /etc/hosts \
        -e "/^127\.0\.1\.1/ c 127.0.1.1\t $hostname"
}

setup_networking() {
    local wan_if=${WAN:-enp0s3}
    local lan_if=${LAN:-enp0s8}

    sed -i /etc/network/interfaces -e "/auto $wan_if/,\$ d"

    if is_gateway; then
        local network=${NETWORK:-192.168.10}
        echo "
            auto $wan_if
            iface $wan_if inet dhcp

            auto $lan_if
            iface $lan_if inet static
                address ${network}.1
                netmask 255.255.255.0
        " >> /etc/network/interfaces
    else
        echo "
            auto $wan_if
            iface $wan_if inet dhcp
        " >> /etc/network/interfaces
        [[ $lan_if != $wan_if ]] && echo "
            auto $lan_if
            iface $lan_if inet dhcp
        " >> /etc/network/interfaces
    fi
}

enable_nat() {
    # enable ip forwarding
    echo 1 > /proc/sys/net/ipv4/ip_forward
    sed -i /etc/sysctl.conf \
        -e '/net.ipv4.ip_forward/d'
    echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf

    # delete any existing iptables rules
    iptables --flush
    iptables --table nat --flush
    iptables --delete-chain
    iptables --table nat --delete-chain

    # add IP masquerading rules
    local wan_if=${WAN:-enp0s3}
    local lan_if=${LAN:-enp0s8}
    iptables -t nat -A POSTROUTING -o $wan_if -j MASQUERADE
    iptables -A FORWARD -i $wan_if -o $lan_if -m state --state RELATED,ESTABLISHED -j ACCEPT
    iptables -A FORWARD -i $lan_if -o $wan_if -j ACCEPT

    # install iptables-persistent and save iptables rules
    apt install --yes iptables-persistent
    iptables-save > /etc/iptables/rules.v4
}

mount_vbox_shared_folders() {
    [[ -d /host ]] || return 0

    sed -i /etc/fstab -e '/^host/d'
    cat <<EOF >> /etc/fstab
host /host vboxsf defaults 0 0
EOF
}

setup_lts_conf() {
    # create lts.conf
    local lts_conf_file=$(ltsp-config -o lts.conf | cut -d' ' -f2)

    # setup guest accounts
    if [[ ${GUEST_ACCOUNTS,,} == "yes" ]]; then
        # enable guest login button
        sed -i $lts_conf_file \
            -e "/LDM_GUESTLOGIN=/ a HOSTNAME_BASE=\"$HOSTNAME\"" \
            -e "/LDM_GUESTLOGIN=/ c LDM_GUESTLOGIN=True"

        # enable autologin for the guest accounts
        [[ ${GUEST_AUTOLOGIN,,} == "yes" ]] \
            && sed -i $lts_conf_file \
                   -e "/LDM_GUESTLOGIN=/ a LDM_AUTOLOGIN=True"

        # create and config guest accounts
        run create-guest-accounts.sh $GUEST_USER "$GUEST_PASS" $HOSTNAME
    fi

    # Fix the problem of sshfs hang, which causes fat client freeze.
    # It is discussed here: https://sourceforge.net/p/ltsp/mailman/message/36596662/
    sed -i $lts_conf_file \
        -e '/LDM_SSHOPTIONS/d' \
        -e '/^\[Default\]/ a LDM_SSHOPTIONS="-o ServerAliveInterval=30"'

    # It is always a good idea to set LDM_PASSWORD_HASH=True
    # This will create a proper shadow entry on the client, allowing for
    # screen locking, and other things which require authentication to work.
    sed -i $lts_conf_file \
        -e '/LDM_PASSWORD_HASH/d' \
        -e '/^\[Default\]/ a LDM_PASSWORD_HASH=True'

    # It is a good idea to set REMOTE_APPS=True
    sed -i $lts_conf_file \
        -e '/REMOTE_APPS/d' \
        -e '/^\[Default\]/ a REMOTE_APPS=True'

    # allow teacher to start Epoptes from a fat client
    sed -i $lts_conf_file \
	-e '/RCFILE_01/d' \
	-e "/REMOTE_APPS/ a RCFILE_01=\"sed -i /usr/share/applications/epoptes.desktop -e '/Exec=/c Exec=ltsp-remoteapps dbus-launch epoptes'\""

    # allow users to change their password from mate-about-me or users-admin
    sed -i $lts_conf_file \
	-e '/RCFILE_02/d' \
	-e '/RCFILE_03/d' \
	-e "/REMOTE_APPS/ a RCFILE_02=\"sed -i /usr/share/applications/mate-about-me.desktop -e '/Exec=/c Exec=ltsp-remoteapps dbus-launch mate-about-me'\"" \
	-e "/REMOTE_APPS/ a RCFILE_03=\"sed -i /usr/share/applications/users.desktop -e '/Exec=/c Exec=ltsp-remoteapps dbus-launch users-admin'\""
}

setup_dnsmasq() {
    local dnsmasq_conf=/etc/dnsmasq.d/ltsp-server-dnsmasq.conf
    if is_gateway; then
        local network=${NETWORK:-192.168.10}
        local dhcp_range=${DHCP_RANGE:-${network}.100,${network}.250}
        ltsp-config dnsmasq --enable-dns
        sed -e '/^dhcp-range=/d' -i $dnsmasq_conf
        echo "dhcp-range=${dhcp_range},8h" >> $dnsmasq_conf
    else
        ltsp-config dnsmasq
	cat <<- EOF > /usr/local/sbin/update-dnsmasq-config.sh
		#!/bin/bash
		lan_if=${LAN:-enp0s8}
		lan_ip=\$(ip addr show \$lan_if | grep -Po 'inet \K[\d.]+')
		network="\${lan_ip%.*}.0"
		sed -e '/^dhcp-range=/d' -i $dnsmasq_conf
		echo dhcp-range=\$network,proxy >> $dnsmasq_conf
		systemctl restart dnsmasq.service
		EOF
        chmod +x /usr/local/sbin/update-dnsmasq-config.sh
        cat <<- EOF > /etc/systemd/system/update-dnsmasq-config.service
		[Unit]
		RequiredBy=dnsmasq.service
		
		[Service]
		ExecStart=/usr/local/sbin/update-dnsmasq-config.sh
		
		[Install]
		WantedBy=default.target
		EOF
	systemctl daemon-reload
	systemctl enable update-dnsmasq-config.service
	systemctl start update-dnsmasq-config.service
    fi
    systemctl restart dnsmasq.service
}

setup_lightdm() {
    # disable autologin
    sed -i /etc/lightdm/lightdm.conf \
        -e '/^autologin-user/d'

    # don't show a list of users on login
    cat <<EOF > /etc/lightdm/lightdm.conf.d/71-hide-users.conf
[SeatDefaults]
greeter-hide-users=true
EOF
}

setup_users() {
    # create test users
    [[ ${DEVELOPMENT,,} == "yes" ]] && cat <<EOF | run users.sh create
user1:pass1
user2:pass2
EOF

    # for security reasons, set a random password for the user vagrant
    local random_pass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w30 | head -n1)
    usermod --password "$(openssl passwd -stdin <<< "$random_pass")" vagrant

    # enable password authentication
    sed -i /etc/ssh/sshd_config \
        -e "/PasswordAuthentication no\$/ c PasswordAuthentication yes"
    systemctl restart ssh
}

install_chat() {
    cp -a $SRC_DIR/scripts/chat/ /var/www/html/
    chown www-data:www-data /var/www/html/chat/chat.txt
    apt install --yes php
    systemctl restart apache2
}

cleanup() {
    mkdir -p /etc/ltsp/cleanup.d
    cat <<'EOF' > /etc/ltsp/cleanup.d/60-cleanup
set -x

# nat
sed -i /etc/sysctl.conf \
    -e '/net.ipv4.ip_forward/ c net.ipv4.ip_forward=0'
rm -f /etc/iptables/rules.v4
apt purge --yes iptables-persistent

# shared folders
sed -i /etc/fstab -e '/vboxfs/d'

# guest accounts
rm -f /usr/local/bin/reset-guest-account.sh
rm -f /etc/sudoers.d/reset-guest-account
rm -f /usr/share/ldm/rc.d/S00-guest-sessions
sed -i /etc/security/limits.conf -e '/^### custom/,$ d'

# dnsmasq
rm -f /etc/dnsmasq.d/ltsp-server-dnsmasq.conf

# guacamole server
build_dependencies="
        build-essential libcairo2-dev libjpeg-turbo8-dev libpng-dev libossp-uuid-dev
        libavcodec-dev libavutil-dev libswscale-dev
        libfreerdp-dev libpango1.0-dev libssh2-1-dev libtelnet-dev
        libvncserver-dev libpulse-dev libssl-dev libvorbis-dev libwebp-dev
        gcc-6 dpkg-dev
    "
apt purge --yes $build_dependencies
build_folder=$(dpkg-architecture -qDEB_BUILD_GNU_TYPE)
rm -f /usr/lib/$build_folder/freerdp/guac*.so

# guacamole client
rm -f /var/lib/tomcat8/webapps/guacamole.war
apt purge --yes tomcat8

# guacamole database
apt purge --yes mysql-server mysql-client mysql-common mysql-utilities libmysql-java
rm -rf /var/lib/mysql

# guacamole config
rm -rf /etc/guacamole

# vnc server
apt purge --yes vnc4server
rm -f /etc/lightdm/lightdm.conf.d/20-vnc.conf

# xrdp
apt purge --yes xrdp xorgxrdp xrdp-pulseaudio-installer
apt purge --yes apt install --yes pulseaudio build-essential dpkg-dev git
rm -f /etc/apt/sources.list.d/bionic-updates-src.list

# apache
apt purge --yes apache2 php
rm -rf /etc/apache2
rm -rf /var/www/html/

# nginx
apt purge --yes nginx
rm -rf /etc/nginx

set +x
EOF
}

# call the main function
main
