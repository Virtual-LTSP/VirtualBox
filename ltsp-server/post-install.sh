#!/bin/bash -x
### This is a hook script that runs after 'install.sh'.
### Feel free to change and customize it if needed.
### Run 'git update-index --skip-worktree ltsp-server/pre-install.sh'
### so that git ignores the changes to this file.

include settings.sh

# install additinal packages
apt update
apt upgrade --yes
apt install --yes --install-recommends \
    x2goserver
