# Set "yes" for development and internal or external testing.
# Set "no" when running on production. See:
# https://gitlab.com/Virtual-LTSP/VirtualBox/wikis/Test-Cases
DEVELOPMENT="yes"

# Set "yes" for gateway mode. Set "no" for normal mode.
# For more details see this:
# https://gitlab.com/Virtual-LTSP/VirtualBox/wikis/LTSP-Modes-of-Operation
GATEWAY="no"

# Network interface that is connected to LAN.
# If set to 'ltsptest01' a dummy interface will be created and used.
# This is useful for internal testing. For more details see this:
# https://gitlab.com/Virtual-LTSP/VirtualBox/wikis/Test-Cases
LAN_IF="ltsptest01"

# Virtual machine configuration. Any box based on Ubuntu-18.04
# can be used. You can find available vagrant boxes here:
# https://app.vagrantup.com/boxes/search
# https://app.vagrantup.com/ltsp
VM_BOX="ltsp/linuxmint-19.2-32bit-edu"
VM_RAM="1024"

# Admin account.
ADMIN_USER="admin"
ADMIN_PASS="pass"

# Set the name of the server.
HOSTNAME="ltsp-server"

# List of extra packages that need to be installed on the client.
# For example: ubuntu-edu-preschool, ubuntu-edu-primary, etc.
PACKAGES="
  vim
  geany
"

# If GUEST_ACCOUNTS is "yes" then guest accounts are created and
# a "Login as Guest" button is provided on the clients. Everything
# on a guest account will be reset on login.
#
# If GUEST_AUTOLOGIN is "yes", then the client will automatically
# login to a guest account as soon as it boots.
#
# The account GUEST_USER is used as a template (skeleton) for the
# guest accounts. You can login to this account in order to customize
# and tweak all the guest accounts.
GUEST_ACCOUNTS="yes"
GUEST_AUTOLOGIN="no"
GUEST_USER="guest"
GUEST_PASS="pass"

# Access the server from the web with Guacamole
# https://guacamole.apache.org/doc/gug/using-guacamole.html
# It can be accessed on: https://127.0.0.1/guac/
# Comment out to disable installing Guacamole.
GUAC_ADMIN="admin"
GUAC_PASS="pass"
GUAC_USER_NAME="student"
GUAC_USER_PASS="student"
GUAC_MAX_CONNECTIONS="50"
#VNC_PORT="5901"
#VNC_WIDTH="1024"
#VNC_HEIGHT="768"
#VNC_DEPTH="24"
