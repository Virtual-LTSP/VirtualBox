#!/bin/bash
### Install LTSP server on a real machine (not on a virtual box).

fail() { echo "$@" >&2; exit 1; }

read -p "
This script installs LTSP server on a real machine (not on a virtual box).
This machine is supposed to have a fresh installation of Ubuntu-18.04 or
a derivative of it, preferably LinuxMint-19.

It is not recommended to run this script on your own laptop or a machine
that is used for some other purpose as well (besides being a LTSP server).
In this case you should run './server.sh build' to create a VirtualBox
machine and install LTSP on it. For more details see README.md or:
https://gitlab.com/Virtual-LTSP/VirtualBox

Are you sure you want to continue? [y/N] " input
[[ $input == [yY] ]] || exit
echo

# make sure that the script is called as root
[[ "$UID" != 0 ]] && fail "
Error: Use sudo or run script as user root.
"

# make sure that we are running on bionic
source /etc/os-release
[[ $UBUNTU_CODENAME == 'bionic' ]] || fail "
Error: These scripts work well on Ubuntu-18.04 (bionic) and its derivatives
    (like LinuxMint-19). For other systems, like Debian or earlier versions
    of Ubuntu, small manual changes/fixes may be required to make them work.
"

# check settings
source settings.sh
[[ ${DEVELOPMENT,,} == 'yes' ]] && fail "
Error: Edit 'settings.sh' and set DEVELOPMENT to \"no\" (or comment it out).
"
[[ $LAN_IF == 'ltsptest01' ]] && fail "
Error: Edit 'settings.sh' and set a propper value for LAN_IF.
"
[[ $HOSTNAME == 'ltsp-server' ]] && fail "
Error: Edit 'settings.sh' and set a propper value for HOSTNAME.
"
[[ $ADMIN_PASS == 'pass' ]] && fail "
Error: ADMIN_PASS on 'settings.sh' has to be changed for security reasons.
"
[[ $GUEST_PASS == 'pass' ]] && fail "
Error: GUEST_PASS on 'settings.sh' has to be changed for security reasons.
"
[[ $GUAC_PASS == 'pass' ]] && fail "
Error: GUAC_PASS on 'settings.sh' has to be changed for security reasons.
"

# check the LAN interface
interfaces="$(ip link | grep -v link | cut -d: -f2 | grep -v lo | xargs echo)"
echo $interfaces | grep $LAN_IF &>/dev/null || fail "
Error: LAN_IF=\"$LAN_IF\" is not a valid interface. It should be
       one of: $interfaces. Edit 'settings.sh' and fix it.
"

# get and check the WAN interface
wan_interfaces="$(ip route | grep default | cut -d' ' -f5 | xargs echo)"
[[ -z $wan_interfaces ]] && fail "
Error: The system is not connected to the internet.
"
read -p "Enter the interface that is connected to the internet [$wan_interfaces]: " input
wan_if=${input:-$(echo $wan_interfaces | cut -d' ' -f1)}
echo $wan_interfaces | grep $wan_if &>/dev/null || fail "
Error: $wan_if is not a valid interface or it does not have
       a default route (to the gateway).
"

# get LAN network and DHCP range
if [[ ${GATEWAY,,} == 'yes' ]]; then
    read -p "Enter the LAN network (for the DHCP server) [192.168.10]: " input
    lan_network=${lan_network:-192.168.10}
    read -p "Enter the DHCP range [$lan_network.100,$lan_network.250]: " input
    dhcp_range=${input:-$lan_network.100,$lan_network.250}
fi

# export variables to customize the installation scripts
export LAN=$LAN_IF
export WAN=$wan_if
export NETWORK=${lan_network:-192.168.10}
export DHCP_RANGE=${dhcp_range:-192.168.10.100,192.168.10.250}

export SRC_DIR="$(dirname $(dirname $0))"
$SRC_DIR/ltsp-server/install.sh
